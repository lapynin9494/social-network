package com.getjavajob.training.lapynina.socialnetwork.dao.dto;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@NoArgsConstructor
public class EventMailer {

    private String senderName;
    private String senderSurname;
    private String emailToSend;
    private String textMessage;
    private Type type;

    public EventMailer(Message message, Account recipient, Account sender) {
        this.senderName = sender.getName();
        this.senderSurname = sender.getSurname();
        this.emailToSend = recipient.getEmail();
        this.textMessage = message.getTextMessage();
        this.type = Type.MESSAGE_WALL;
    }

    public EventMailer(Account account, Account friend) {
        this.senderName = friend.getName();
        this.senderSurname = friend.getSurname();
        this.emailToSend = account.getEmail();
        this.type = Type.FRIEND_REQUEST;
    }

    public enum Type {
        MESSAGE_WALL,
        FRIEND_REQUEST
    }

}