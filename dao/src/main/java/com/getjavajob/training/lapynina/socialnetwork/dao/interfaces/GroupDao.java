package com.getjavajob.training.lapynina.socialnetwork.dao.interfaces;

import com.getjavajob.training.lapynina.socialnetwork.common.Group;

import java.util.List;

public interface GroupDao extends EntityDao<Group> {

    List<Group> getAll();

}