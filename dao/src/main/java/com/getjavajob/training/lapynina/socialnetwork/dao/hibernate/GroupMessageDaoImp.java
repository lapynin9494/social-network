package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.common.MessageGroup;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.MessageDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class GroupMessageDaoImp extends AbstractEntityDao<MessageGroup> implements MessageDao<MessageGroup> {
    private static final Logger logger = LogManager.getLogger();

    public GroupMessageDaoImp(EntityManager entityManager) {
        super(MessageGroup.class, entityManager);
    }

    @Override
    public List<MessageGroup> getByAppointment(int recipientId, String appointment) {
        try {
            return entityManager.createQuery("select mg FROM MessageGroup mg"
                            + " WHERE mg.recipientId =:recipientId AND mg.appointment =:appointment"
                            + " ORDER BY mg.timeSend DESC", MessageGroup.class)
                    .setParameter("recipientId", recipientId)
                    .setParameter("appointment", appointment)
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

}