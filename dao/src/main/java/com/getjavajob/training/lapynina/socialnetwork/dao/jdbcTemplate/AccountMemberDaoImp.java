package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.AccountRowMapper;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.MemberDao;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

@AllArgsConstructor
//@Repository
@PropertySource("classpath:queries")
public class AccountMemberDaoImp implements MemberDao {
    public JdbcTemplate jdbcTemplate;
    public Environment environment;

    public RowMapper<Account> setRowMapper() {
        return new AccountRowMapper();
    }

    @Override
    public void invite(int inviterId, int friendId) {
        jdbcTemplate.update(environment.getProperty("account.invite"), inviterId, friendId);
    }

    @Override
    public List<Account> getInvitesAccount(int accountId) {
        return jdbcTemplate.query(environment.getProperty("account.getInvites"), setRowMapper(), accountId);
    }

    @Override
    public boolean acceptInvite(int accountId, int friendId) {
        jdbcTemplate.update(environment.getProperty("account.addFriend"), accountId, friendId, friendId, accountId);
        jdbcTemplate.update(environment.getProperty("account.deleteFromInvitesAfterAdd"), friendId, accountId);
        return true;
    }

    @Override
    public void rejectInvite(int inviterId, int accountId) {
        jdbcTemplate.update(environment.getProperty("account.deleteFromInvitesAfterAdd"), inviterId, accountId);
    }

    @Override
    public List<Account> getMembers(int accountId) {
        return jdbcTemplate.query(environment.getProperty("account.getFriend"), setRowMapper(), accountId);
    }

    @Override
    public boolean delete(int accountId, int friendId) {
        jdbcTemplate.update(environment.getProperty("account.deleteFriend"), accountId, friendId);
        jdbcTemplate.update(environment.getProperty("account.deleteFriend"), friendId, accountId);
        return true;
    }

    public List<Account> getOutgoingInvites(int accountId) {
        return jdbcTemplate.query(environment.getProperty("account.getOutgoingInvites"), setRowMapper(), accountId);
    }

}