package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.AccountRowMapper;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.AccountDao;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

//@Repository
@PropertySource("classpath:queries")
public class AccountDaoImp extends AbstractEntityDao<Account> implements AccountDao {

    public AccountDaoImp(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate, Environment environment) {
        super(jdbcTemplate, namedJdbcTemplate, environment);
    }

    @Override
    public RowMapper<Account> setRowMapper() {
        return new AccountRowMapper();
    }

    @Override
    public String getInsertQuery() {
        return environment.getProperty("account.create");
    }

    @Override
    public String getGetQuery() {
        return environment.getProperty("account.getById");
    }

    @Override
    public String getUpdateQuery() {
        return environment.getProperty("account.update");
    }

    @Override
    public String getDeleteQuery() {
        return environment.getProperty("account.delete");
    }


    public List<Account> getBySubSrt(String subStr, int pagenumber) {
        String subName = "%" + subStr + "%";
        return jdbcTemplate.query(environment.getProperty("account.getBySubStr"), setRowMapper(), subName, subName);
    }

    @Override
    public List<Account> getAll() {
        return jdbcTemplate.query(environment.getProperty("account.getAll"), setRowMapper());
    }

    @Override
    public Account getByEmail(String email) {
        return jdbcTemplate.query(environment.getProperty("account.getByEmail"), setRowMapper()
                , new Object[]{email}).stream().findAny().orElse(null);
    }

}