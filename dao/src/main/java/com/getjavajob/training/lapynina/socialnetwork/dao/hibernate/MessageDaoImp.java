package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.MessageDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class MessageDaoImp extends AbstractEntityDao<Message> implements MessageDao<Message> {
    private static final Logger logger = LogManager.getLogger();

    public MessageDaoImp(EntityManager entityManager) {
        super(Message.class, entityManager);
    }

    @Override
    public List<Message> getByAppointment(int recipientId, String appointment) {
        try {
            return entityManager.createQuery("SELECT m FROM Message m WHERE m.recipientId =: recipientId AND m.appointment =: appointment ORDER BY m.timeSend DESC", Message.class)
                    .setParameter("recipientId", recipientId)
                    .setParameter("appointment", appointment)
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

}