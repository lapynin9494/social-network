package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.EntityDao;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@AllArgsConstructor
public class AbstractEntityDao<T> implements EntityDao<T> {
    private Class<T> clazz;
    @PersistenceContext
    public EntityManager entityManager;
    private static final Logger logger = LogManager.getLogger();

    @Override
    public T create(T entityRequest) {
        try {
            entityManager.persist(entityRequest);
            return entityRequest;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public T getById(int id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public T update(T entityRequest) {
        return entityManager.merge(entityRequest);
    }

    @Override
    public boolean deleteById(int id) {
        try {
            T entity = entityManager.find(clazz, id);
            entityManager.remove(entity);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
            return false;
        }
    }

}