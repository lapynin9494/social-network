package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.MemberDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class GroupMemberDaoImp extends AbstractEntityDao<Group> implements MemberDao {
    private static final Logger logger = LogManager.getLogger();
    private AccountDaoImp accountDaoImp;

    @Autowired
    public GroupMemberDaoImp(EntityManager entityManager, AccountDaoImp accountDaoImp) {
        super(Group.class, entityManager);
        this.accountDaoImp = accountDaoImp;
    }

    @Override
    public boolean acceptInvite(int inviterId, int groupId) {
        try {
            Account account = accountDaoImp.getById(inviterId);
            Group group = getById(groupId);
            account.getGroupInvite().remove(group);
            account.getAccountGroup().add(group);
            entityManager.merge(account);
            return true;
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public List<Account> getMembers(int groupId) {
        try {
            Group groupWithMembers = entityManager.createQuery("SELECT g FROM Group g"
                                    + " LEFT JOIN FETCH g.members WHERE g.id = :id"
                            , Group.class)
                    .setParameter("id", groupId)
                    .getSingleResult();
            return groupWithMembers.getMembers();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean delete(int memberId, int groupId) {
        try {
            Account account = accountDaoImp.getById(memberId);
            Group group = getById(groupId);
            account.getAccountGroup().remove(group);
            entityManager.merge(account);
            return true;
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public void invite(int inviterId, int groupId) {
        try {
            Account account = accountDaoImp.getById(inviterId);
            Group group = getById(groupId);
            account.getGroupInvite().add(group);
            entityManager.merge(account);
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public List<Account> getInvitesAccount(int groupId) {
        try {
            Group group = getById(groupId);
            return group.getInviter();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void rejectInvite(int inviterId, int groupId) {
        try {
            Account account = accountDaoImp.getById(inviterId);
            Group group = getById(groupId);
            account.getGroupInvite().remove(group);
            entityManager.merge(account);
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public List<Group> getOutgoingInvites(int accountId) {
        try {
            Account accountWithGroupInvite = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.groupInvite WHERE a.id = :id"
                            , Account.class)
                    .setParameter("id", accountId)
                    .getSingleResult();
            return accountWithGroupInvite.getGroupInvite();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

}