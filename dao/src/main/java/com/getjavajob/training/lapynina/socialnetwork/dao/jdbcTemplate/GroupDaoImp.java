package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.GroupRowMapper;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.GroupDao;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

//@Repository
@PropertySource("classpath:queries")
public class GroupDaoImp extends AbstractEntityDao<Group> implements GroupDao {

    public GroupDaoImp(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate, Environment environment) {
        super(jdbcTemplate, namedJdbcTemplate, environment);
    }

    @Override
    public RowMapper<Group> setRowMapper() {
        return new GroupRowMapper();
    }

    @Override
    public String getInsertQuery() {
        return environment.getProperty("group.create");
    }

    @Override
    public String getGetQuery() {
        return environment.getProperty("group.getById");
    }

    @Override
    public String getDeleteQuery() {
        return environment.getProperty("group.deleteById");
    }

    @Override
    public String getUpdateQuery() {
        return environment.getProperty("group.updateById");
    }

    @Override
    public List<Group> getAll() {
        return jdbcTemplate.query(environment.getProperty("group.getAll"), setRowMapper());
    }

    public List<Group> getBySubSrt(String subStr) {
        String subName = "%" + subStr + "%";
        return jdbcTemplate.query(environment.getProperty("group.getBySubStr"), setRowMapper(), subName);
    }

}