package com.getjavajob.training.lapynina.socialnetwork.dao.interfaces;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;

import java.util.List;

public interface AccountDao extends EntityDao<Account> {

    Account getByEmail(String email);

    List<Account> getAll();

}
