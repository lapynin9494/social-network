package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.MemberDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.Set;

@Repository
public class AccountMemberDaoImp extends AbstractEntityDao<Account> implements MemberDao {
    private static final Logger logger = LogManager.getLogger();

    public AccountMemberDaoImp(EntityManager entityManager) {
        super(Account.class, entityManager);
    }

    @Override
    public void invite(int friendId, int inviterId) {
        try {
            Account accountWithInvites = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.accountInvites WHERE a.id = :friendId"
                            , Account.class)
                    .setParameter("friendId", friendId)
                    .getSingleResult();
            accountWithInvites.getAccountOutgoingInvites().add(getById(inviterId));
            entityManager.merge(accountWithInvites);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public List<Account> getInvitesAccount(int accountId) {
        try {
            Account accountWithInvites = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.accountInvites WHERE a.id = :accountId"
                            , Account.class)
                    .setParameter("accountId", accountId)
                    .getSingleResult();
            return accountWithInvites.getAccountInvites();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public List<Account> getOutgoingInvites(int accountId) {
        try {
            Account accountWithOutgoingInvites = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.accountOutgoingInvites WHERE a.id = :accountId"
                            , Account.class)
                    .setParameter("accountId", accountId)
                    .getSingleResult();
            return accountWithOutgoingInvites.getAccountOutgoingInvites();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean acceptInvite(int accountId, int friendId) {
        try {
            Account account = getById(accountId);
            Account accountFriend = getById(friendId);
            account.getAccountInvites().remove(accountFriend);
            account.getAccountFriends().add(accountFriend);
            accountFriend.getAccountOutgoingInvites().remove(account);
            accountFriend.getAccountFriends().add(account);
            entityManager.merge(account);
            entityManager.merge(accountFriend);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public void rejectInvite(int inviterId, int accountId) {
        try {
            Account account = getById(accountId);
            account.getAccountInvites().remove(getById(inviterId));
            entityManager.merge(account);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public List<Account> getMembers(int accountId) {
        try {
            Account accountWithFriends = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.accountFriends WHERE a.id = :id"
                            , Account.class)
                    .setParameter("id", accountId)
                    .getSingleResult();
            return accountWithFriends.getAccountFriends();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public Set<Group> getAccountGroup(int accountId) {
        try {
            Account accountWithFriends = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.accountGroup WHERE a.id = :id"
                            , Account.class)
                    .setParameter("id", accountId)
                    .getSingleResult();
            return accountWithFriends.getAccountGroup();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public boolean delete(int accountId, int friendId) {
        try {
            Account account = getById(accountId);
            Account accountFriend = getById(friendId);
            account.getAccountFriends().remove(accountFriend);
            accountFriend.getAccountFriends().remove(account);
            entityManager.merge(account);
            entityManager.merge(accountFriend);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

}