package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.MemberDao;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

@AllArgsConstructor
//@Repository
@PropertySource("classpath:queries")
public class GroupMemberDaoImp implements MemberDao {
    public JdbcTemplate jdbcTemplate;
    public Environment environment;
    public AccountDaoImp accountDaoImp;
    public GroupDaoImp groupDaoImp;

    @Override
    public boolean acceptInvite(int inviterId, int groupId) {
        jdbcTemplate.update(environment.getProperty("group.addMember"), groupId, inviterId);
        jdbcTemplate.update(environment.getProperty("group.deleteFromInvitesAfterAdd"), inviterId, groupId);
        return true;
    }

    @Override
    public List<Account> getMembers(int groupId) {
        return jdbcTemplate.query(environment.getProperty("group.getMembers"), accountDaoImp.setRowMapper(), groupId);
    }

    @Override
    public boolean delete(int memberId, int groupId) {
        jdbcTemplate.update(environment.getProperty("group.deleteMember"), memberId, groupId);
        return true;
    }

    @Override
    public void invite(int inviterId, int groupId) {
        jdbcTemplate.update(environment.getProperty("group.invite"), inviterId, groupId);
    }

    @Override
    public List<Account> getInvitesAccount(int groupId) {
        return jdbcTemplate.query(environment.getProperty("group.getInvites"), accountDaoImp.setRowMapper(), groupId);
    }

    @Override
    public void rejectInvite(int inviterId, int groupId) {
        jdbcTemplate.update(environment.getProperty("group.deleteFromInvitesAfterAdd"), inviterId, groupId);
    }

    public List<Group> getOutgoingInvites(int accountId) {
        return jdbcTemplate.query(environment.getProperty("group.getOutgoingInvites"), groupDaoImp.setRowMapper(), accountId);
    }

}
