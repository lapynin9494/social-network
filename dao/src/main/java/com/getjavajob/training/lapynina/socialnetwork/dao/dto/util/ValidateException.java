package com.getjavajob.training.lapynina.socialnetwork.dao.dto.util;

public class ValidateException extends Exception {

    public ValidateException(String msg) {
        super(msg);
    }

}