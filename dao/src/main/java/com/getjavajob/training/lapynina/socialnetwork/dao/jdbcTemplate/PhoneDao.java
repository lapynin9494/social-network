package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;

//@Repository
@AllArgsConstructor
@PropertySource("classpath:queries")
public class PhoneDao {
    private JdbcTemplate jdbcTemplate;
    private Environment environment;

    private String createFromBase(ResultSet resultSet) throws SQLException {
        return resultSet.getString("PHONE");
    }

    private String workPhoneCreateFromBase(ResultSet resultSet) throws SQLException {
        return resultSet.getString("WORKPHONE");
    }

    public int create(int id, String phone, String workPhone) {
        jdbcTemplate.update(environment.getProperty("phone.create"), id, phone, workPhone);
        return id;
    }

    public String getById(int id) {
        return jdbcTemplate.query(environment.getProperty("phone.getById"), (rs, rowNum) -> createFromBase(rs)
                , new Object[]{id}).stream().findAny().orElse(null);
    }

    public String getWorkPhoneById(int id) {
        return jdbcTemplate.query(environment.getProperty("workPhone.getById"), (rs, rowNum) -> workPhoneCreateFromBase(rs)
                , new Object[]{id}).stream().findAny().orElse(null);
    }

    public int update(int id, String phone, String workPhone) {
        jdbcTemplate.update(environment.getProperty("phone.update"), phone, workPhone, id);
        return id;
    }

    public boolean deleteById(int id) {
        jdbcTemplate.update(environment.getProperty("phone.delete"), id);
        return true;
    }

}