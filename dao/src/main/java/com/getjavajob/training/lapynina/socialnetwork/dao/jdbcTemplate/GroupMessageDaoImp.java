package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.MessageRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

//@Repository
@PropertySource("classpath:queries")
public class GroupMessageDaoImp extends AbstractEntityDao<Message> {

    @Autowired
    public GroupMessageDaoImp(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate, Environment environment) {
        super(jdbcTemplate, namedJdbcTemplate, environment);
    }

    @Override
    public RowMapper<Message> setRowMapper() {
        return new MessageRowMapper();
    }

    @Override
    public String getInsertQuery() {
        return environment.getProperty("messageGroup.create");
    }

    @Override
    public String getGetQuery() {
        return environment.getProperty("messageGroup.getById");
    }

    @Override
    public String getUpdateQuery() {
        return environment.getProperty("messageGroup.update");
    }

    @Override
    public String getDeleteQuery() {
        return environment.getProperty("messageGroup.delete");
    }

    public List<Message> getByAppointment(int recipientId, String appointment) {
        return jdbcTemplate.query(environment.getProperty("messageGroup.getByAppointment"), setRowMapper(), recipientId, appointment);
    }

}
