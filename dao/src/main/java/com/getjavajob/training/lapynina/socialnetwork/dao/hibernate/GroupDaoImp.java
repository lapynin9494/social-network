package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.GroupDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class GroupDaoImp extends AbstractEntityDao<Group> implements GroupDao {
    private static final Logger logger = LogManager.getLogger();
    private static final int MAX_SEARCH_ITEMS = 5;

    public GroupDaoImp(EntityManager entityManager) {
        super(Group.class, entityManager);
    }

    @Override
    public List<Group> getAll() {
        try {
            return entityManager.createQuery("FROM Group", Group.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public List<Group> getBySubSrt(String subStr, String pageNumber) {
        try {
            return entityManager.createQuery("SELECT g FROM Group g WHERE g.name LIKE :substring", Group.class)
                    .setFirstResult(MAX_SEARCH_ITEMS * (Integer.parseInt(pageNumber)))
                    .setMaxResults(MAX_SEARCH_ITEMS)
                    .setParameter("substring", "%" + subStr + "%")
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public int getBySubSrtCount(String subStr) {
        try {
            int subSrtCount = entityManager.createQuery("SELECT g FROM Group g WHERE g.name LIKE :substring", Group.class)
                    .setParameter("substring", "%" + subStr + "%")
                    .getResultList().size();
            return (subSrtCount / MAX_SEARCH_ITEMS) + 1;
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }

    public List<Group> getBySubSrt(String subStr) {
        try {
            return entityManager.createQuery("SELECT g FROM Group g WHERE g.name LIKE :substring", Group.class)
                    .setParameter("substring", "%" + subStr + "%")
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

}