package com.getjavajob.training.lapynina.socialnetwork.dao.interfaces;

import java.util.List;

public interface MessageDao<T> extends EntityDao<T> {

    List<T> getByAppointment(int idRecipient, String appointment);

}