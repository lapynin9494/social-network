package com.getjavajob.training.lapynina.socialnetwork.dao.hibernate;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.AccountDto;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.AccountMapper;
import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.AccountDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

@Repository
public class AccountDaoImp extends AbstractEntityDao<Account> implements AccountDao {
    private static final Logger logger = LogManager.getLogger();
    private static final int MAX_SEARCH_ITEMS = 5;

    @Autowired
    private AccountMapper mapper;

    public AccountDaoImp(EntityManager entityManager) {
        super(Account.class, entityManager);
    }

    public List<Account> getBySubSrt(String subStr, String pageNumber) {
        try {
            return entityManager.createQuery("SELECT a FROM Account a WHERE a.name LIKE :substring", Account.class)
                    .setFirstResult(MAX_SEARCH_ITEMS * (Integer.parseInt(pageNumber)))
                    .setMaxResults(MAX_SEARCH_ITEMS)
                    .setParameter("substring", "%" + subStr + "%")
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public List<Account> getBySubSrt(String subStr) {
        try {
            return entityManager.createQuery("SELECT a FROM Account a WHERE a.name LIKE :substring", Account.class)
                    .setParameter("substring", "%" + subStr + "%")
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public int getBySubSrtCount(String subStr) {
        try {
            int subSrtCount = entityManager.createQuery("SELECT a FROM Account a WHERE a.name LIKE :substring", Account.class)
                    .setParameter("substring", "%" + subStr + "%")
                    .getResultList().size();
            return (subSrtCount / MAX_SEARCH_ITEMS) + 1;
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
    }

    public Set<Group> getAccountGroupsList(int id) {
        try {
            Account accountWithGroups = entityManager.createQuery("SELECT a FROM Account a"
                                    + " LEFT JOIN FETCH a.accountGroup WHERE a.id = :id"
                            , Account.class)
                    .setParameter("id", id)
                    .getSingleResult();
            return accountWithGroups.getAccountGroup();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public List<Account> getAll() {
        try {
            return entityManager.createQuery("FROM Account", Account.class)
                    .getResultList();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public Account getByEmail(String email) {
        try {
            return entityManager.createQuery("SELECT a FROM Account a WHERE a.email = :email", Account.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public void convertToXml(int id) throws JAXBException {
        Account account = getById(id);
        AccountDto accountDto = mapper.accountToService(account);
        JAXBContext jaxbContext = JAXBContext.newInstance(AccountDto.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(accountDto, new File("D:/xml/account" + id + ".xml"));
    }

    public Account convertFromXml(int id, InputStream fileInputStream) throws JAXBException, FileNotFoundException {
        Account account = getById(id);
        JAXBContext jaxbContext = JAXBContext.newInstance(AccountDto.class);
        Unmarshaller un = jaxbContext.createUnmarshaller();
        AccountDto accountDtoFromXml = (AccountDto) un.unmarshal(fileInputStream);
        Account accountFromXml = mapper.accountToData(accountDtoFromXml);
        return mapFromXml(account, accountFromXml);
    }

    private Account mapFromXml(Account account, Account accountFromXml) {
        account.setName(accountFromXml.getName());
        account.setSurname(accountFromXml.getSurname());
        account.setPatronymic(accountFromXml.getPatronymic());
        account.setAddress(accountFromXml.getAddress());
        account.setWorkAddress(accountFromXml.getWorkAddress());
        account.setPhone(accountFromXml.getPhone());
        account.setWorkPhone(accountFromXml.getWorkPhone());
        account.setIcq(accountFromXml.getIcq());
        account.setAdditionally(accountFromXml.getAdditionally());
        account.setBirthDay(accountFromXml.getBirthDay());
        return account;
    }

}