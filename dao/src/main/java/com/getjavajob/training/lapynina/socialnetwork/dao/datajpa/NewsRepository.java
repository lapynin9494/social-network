package com.getjavajob.training.lapynina.socialnetwork.dao.datajpa;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.News;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

import static java.lang.Double.parseDouble;
import static java.lang.String.valueOf;

@Repository
@AllArgsConstructor
public class NewsRepository {
    private final static int MAX_ELEMENT_PAGE = 5;
    private RedisTemplate<String, Integer> redisTemplate;

    public void save(List<Account> friends, News news) {
        friends.forEach(friend ->
                redisTemplate.opsForZSet().add(valueOf(friend.getId()), news.getId(), parseDouble(news.getTimeSend())));
    }

    public Set<Integer> getNewsForUser(int userId, int page) {
        return redisTemplate.opsForZSet().reverseRange(String.valueOf(userId), (long) (page - 1) * MAX_ELEMENT_PAGE, (long) page * MAX_ELEMENT_PAGE);
    }

}