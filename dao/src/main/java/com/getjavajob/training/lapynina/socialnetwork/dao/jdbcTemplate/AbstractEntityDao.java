package com.getjavajob.training.lapynina.socialnetwork.dao.jdbcTemplate;

import com.getjavajob.training.lapynina.socialnetwork.dao.interfaces.EntityDao;
import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

//@Repository
@AllArgsConstructor
public abstract class AbstractEntityDao<T> implements EntityDao<T> {
    public JdbcTemplate jdbcTemplate;
    public NamedParameterJdbcTemplate namedJdbcTemplate;
    public Environment environment;

    public abstract RowMapper<T> setRowMapper();

    @Override
    public T create(T entityRequest) {
        KeyHolder holder = new GeneratedKeyHolder();
        namedJdbcTemplate.update(getInsertQuery(), new BeanPropertySqlParameterSource(entityRequest), holder);
        return getById((int) holder.getKeys().get("id"));
    }

    public abstract String getInsertQuery();

    @Override
    public T getById(int id) {
        return jdbcTemplate.query(getGetQuery(), setRowMapper()
                , new Object[]{id}).stream().findAny().orElse(null);
    }

    public abstract String getGetQuery();

    @Override
    public T update(T entityRequest) {
        KeyHolder holder = new GeneratedKeyHolder();
        int id = namedJdbcTemplate.update(getUpdateQuery(), new BeanPropertySqlParameterSource(entityRequest), holder);
        return getById(id);
    }

    public abstract String getUpdateQuery();

    @Override
    public boolean deleteById(int id) {
        jdbcTemplate.update(getDeleteQuery(), id);
        return true;
    }

    public abstract String getDeleteQuery();

}
