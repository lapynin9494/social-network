package com.getjavajob.training.lapynina.socialnetwork.dao;

import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.GroupRepositoryImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import resources.DaoConfig;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = DaoConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@Sql(value = "classpath:create_tbl.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:drop_tbl.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class GroupRepositoryTest {
    @Autowired
    private GroupRepositoryImp groupRepositoryImp;

    private Group createGroup() {
        return Group.builder()
                .name("Java")
                .description("sport")
                .idCreator(1)
                .creationDate(LocalDate.parse("2005-10-21"))
                .build();
    }

    @Test
    public void createGroupAndGetById() {
        Group group = createGroup();
        groupRepositoryImp.create(group);
        Group groupData = groupRepositoryImp.getById(1);
        assertEquals(group, groupData);
    }

    @Test
    public void getAllTest() {
        Group group = createGroup();
        groupRepositoryImp.create(group);
        List<Group> groupList = new ArrayList<>();
        groupList.add(group);
        List<Group> groupListData = groupRepositoryImp.getAll();
        assertEquals(groupList, groupListData);
    }

    @Test
    public void updateTest() {
        Group group = createGroup();
        groupRepositoryImp.create(group);
        group.setName("Новое");
        groupRepositoryImp.update(group);
        Group groupTest = Group.builder()
                .name("Новое")
                .description("sport")
                .idCreator(1)
                .creationDate(LocalDate.parse("2005-10-21"))
                .build();
        Group groupData = groupRepositoryImp.getById(1);
        assertEquals(groupTest, groupData);
    }

    @Test
    public void deleteByIdTest() {
        Group group = createGroup();
        groupRepositoryImp.create(group);
        List<Group> groupList = new ArrayList<>();
        groupRepositoryImp.deleteById(1);
        List<Group> groupListData = groupRepositoryImp.getAll();
        assertEquals(groupList, groupListData);
    }

}