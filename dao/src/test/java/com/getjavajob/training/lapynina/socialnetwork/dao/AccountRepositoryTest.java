package com.getjavajob.training.lapynina.socialnetwork.dao;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Roles;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.AccountRepositoryImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import resources.DaoConfig;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = DaoConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@Sql(value = "classpath:create_tbl.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:drop_tbl.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class AccountRepositoryTest {
    @Autowired
    private AccountRepositoryImp accountRepositoryImp;

    private Account createAccount() {
        return Account.builder()
                .surname("Vasin")
                .name("Igor")
                .patronymic("ivanov")
                .email("samara@mail.ru")
                .password("147")
                .birthDay(LocalDate.parse("2005-10-21"))
                .phone("9853671523")
                .role(Roles.USER)
                .build();
    }

    @Test
    public void createAccountAndGetByEmailTest() {
        Account account = createAccount();
        accountRepositoryImp.create(account);
        Account accountData = accountRepositoryImp.getByEmail("samara@mail.ru");
        assertEquals(account, accountData);
    }

    @Test
    public void getByIdTest() {
        Account account = createAccount();
        accountRepositoryImp.create(account);
        Account accountData = accountRepositoryImp.getById(1);
        assertEquals(account, accountData);
    }

    @Test
    public void getAllTest() {
        Account account = createAccount();
        accountRepositoryImp.create(account);
        List<Account> accountList = new ArrayList<>();
        accountList.add(account);
        List<Account> accountListData = accountRepositoryImp.getAll();
        assertEquals(accountList, accountListData);
    }

    @Test
    public void updateTest() {
        Account account = createAccount();
        accountRepositoryImp.create(account);
        account.setName("Новое");
        accountRepositoryImp.update(account);
        Account accountTest = Account.builder()
                .surname("Vasin")
                .name("Новое")
                .patronymic("ivanov")
                .email("samara@mail.ru")
                .password("147")
                .birthDay(LocalDate.parse("2005-10-21"))
                .phone("9853671523")
                .role(Roles.USER)
                .build();
        Account accountData = accountRepositoryImp.getByEmail("samara@mail.ru");
        assertEquals(accountTest, accountData);
    }

    @Test
    public void deleteByIdTest() {
        Account account = createAccount();
        accountRepositoryImp.create(account);
        List<Account> accountList = new ArrayList<>();
        accountRepositoryImp.deleteById(1);
        List<Account> accountListData = accountRepositoryImp.getAll();
        assertEquals(accountList, accountListData);
    }

    @Test
    public void makeAdminTest() {
        Account account = createAccount();
        accountRepositoryImp.create(account);
        accountRepositoryImp.makeAdmin(1);
        Account accountTest = createAccount();
        accountTest.setRole(Roles.ADMIN);
        assertEquals(accountTest, accountTest);
    }

}