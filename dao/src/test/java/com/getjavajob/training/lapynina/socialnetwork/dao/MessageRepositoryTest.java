package com.getjavajob.training.lapynina.socialnetwork.dao;

import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.MessageRepositoryImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import resources.DaoConfig;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ContextConfiguration(classes = DaoConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@Sql(value = "classpath:create_tbl.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:drop_tbl.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class MessageRepositoryTest {
    @Autowired
    private MessageRepositoryImp messageRepositoryImp;

    private Message createMessage() {
        return Message.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("hi")
                .appointment("wall").build();
    }

    @Test
    public void createAndGetByIdTest() {
        Message message = createMessage();
        messageRepositoryImp.create(message);
        Message messageData = messageRepositoryImp.getById(1);
        assertEquals(message, messageData);
    }

    @Test
    public void updateTest() {
        Message message = createMessage();
        messageRepositoryImp.create(message);
        message.setTextMessage("bye");
        messageRepositoryImp.update(message);
        Message messageTest = Message.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("bye")
                .appointment("wall").build();
        Message messageData = messageRepositoryImp.getById(1);
        assertEquals(messageTest, messageData);
    }

    @Test
    public void deleteByIdTest() {
        Message message = createMessage();
        messageRepositoryImp.create(message);
        messageRepositoryImp.deleteById(1);
        assertNull(messageRepositoryImp.getById(1));
    }

    @Test
    public void getByAppointmentTest() {
        Message message = createMessage();
        List<Message> messagesTest = new ArrayList<>();
        messagesTest.add(message);
        messageRepositoryImp.create(message);
        List<Message> messageData = messageRepositoryImp.getByAppointment(1, "wall");
        assertEquals(messagesTest, messageData);
    }

}