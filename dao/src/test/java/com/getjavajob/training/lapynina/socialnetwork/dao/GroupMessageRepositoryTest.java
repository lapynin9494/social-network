package com.getjavajob.training.lapynina.socialnetwork.dao;

import com.getjavajob.training.lapynina.socialnetwork.common.MessageGroup;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.GroupMessageRepositoryImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import resources.DaoConfig;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ContextConfiguration(classes = DaoConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@Sql(value = "classpath:create_tbl.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:drop_tbl.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class GroupMessageRepositoryTest {
    @Autowired
    private GroupMessageRepositoryImp groupMessageRepository;

    private MessageGroup createMessage() {
        return MessageGroup.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("hi")
                .appointment("wall").build();
    }

    @Test
    public void createAndGetByIdTest() {
        MessageGroup message = createMessage();
        groupMessageRepository.create(message);
        MessageGroup messageData = groupMessageRepository.getById(1);
        assertEquals(message, messageData);
    }

    @Test
    public void updateTest() {
        MessageGroup message = createMessage();
        groupMessageRepository.create(message);
        message.setTextMessage("bye");
        groupMessageRepository.update(message);
        MessageGroup messageTest = MessageGroup.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("bye")
                .appointment("wall").build();
        MessageGroup messageData = groupMessageRepository.getById(1);
        assertEquals(messageTest, messageData);
    }

    @Test
    public void deleteByIdTest() {
        MessageGroup message = createMessage();
        MessageGroup messageData = groupMessageRepository.create(message);
        groupMessageRepository.deleteById(messageData.getId());
        assertNull(groupMessageRepository.getById(messageData.getId()));
    }

    @Test
    public void getByAppointmentTest() {
        MessageGroup message = createMessage();
        List<MessageGroup> messagesTest = new ArrayList<>();
        messagesTest.add(message);
        groupMessageRepository.create(message);
        List<MessageGroup> messageData = groupMessageRepository.getByAppointment(1, "wall");
        assertEquals(messagesTest, messageData);
    }

}