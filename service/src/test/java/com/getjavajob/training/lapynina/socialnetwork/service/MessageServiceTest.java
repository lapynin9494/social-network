package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.MessageRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import com.getjavajob.training.lapynina.socialnetwork.service.resources.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ServiceConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@AutoConfigureMockMvc
public class MessageServiceTest {
    @InjectMocks
    MessageService messageService;
    @Mock
    MessageRepositoryImp messageRepositoryImp;
    @Mock
    MessageValidator validator;

    private Message createMessage() {
        return Message.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("hi")
                .appointment("wall").build();
    }

    @Test
    public void createMessageTest() throws ValidateException {
        Message message = createMessage();
        when(messageRepositoryImp.create(message)).thenReturn(message);
        assertEquals(message, messageService.create(message));
    }

    @Test
    public void updateMessageTest() throws ValidateException, SQLException {
        Message message = createMessage();
        when(messageRepositoryImp.create(message)).thenReturn(message);
        when(messageRepositoryImp.getById(message.getId())).thenReturn(message);
        assertEquals(message, messageService.update(message));
    }

    @Test
    public void getByIdTest() {
        Message message = createMessage();
        when(messageRepositoryImp.getById(message.getId())).thenReturn(message);
        assertEquals(message, messageService.getById(message.getId()));
    }

    @Test
    public void getByAppointmentTest() {
        Message message = createMessage();
        List<Message> messages = new ArrayList<>();
        messages.add(message);
        when(messageRepositoryImp.getByAppointment(1, "wall")).thenReturn(messages);
        assertEquals(messages, messageService.getByAppointment(1, "wall"));
    }

    @Test
    public void deleteByIdTest() {
        Message message = createMessage();
        when(messageRepositoryImp.deleteById(message.getId())).thenReturn(true);
        assertTrue(messageService.deleteById(message.getId()));
    }

}