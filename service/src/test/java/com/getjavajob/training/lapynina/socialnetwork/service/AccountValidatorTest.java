package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Roles;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import com.getjavajob.training.lapynina.socialnetwork.service.resources.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = ServiceConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
public class AccountValidatorTest {
    @Autowired
    private AccountValidator validator;

    private Account createAccount() {
        return Account.builder()
                .surname("Vasin")
                .name("Igor")
                .patronymic("ivanov")
                .email("samara@mail.ru")
                .password("147")
                .birthDay(LocalDate.parse("2005-10-21"))
                .phone("9853671523")
                .workPhone("9853671523")
                .address("Spb")
                .workAddress("Spb")
                .skype("40")
                .icq("5697")
                .additionally("")
                .role(Roles.USER)
                .build();
    }

    @Test
    public void validateTrueTest() throws ValidateException {
        Account account = createAccount();
        assertEquals(account, validator.validateAccount(account));
    }

    @Test(expected = ValidateException.class)
    public void validateFalseTest() throws ValidateException {
        Account account = createAccount();
        account.setSurname(null);
        validator.validateAccount(account);
    }

}
