package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import com.getjavajob.training.lapynina.socialnetwork.service.resources.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = ServiceConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
public class MessageValidatorTest {
    @Autowired
    MessageValidator messageValidator;

    private Message createMessage() {
        return Message.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("hi")
                .appointment("wall").build();
    }

    @Test
    public void validateTrueTest() throws ValidateException {
        Message message = createMessage();
        assertEquals(message, messageValidator.validateMessage(message));
    }

    @Test(expected = ValidateException.class)
    public void validateFalseTest() throws ValidateException {
        Message message = createMessage();
        message.setTextMessage(null);
        messageValidator.validateMessage(message);
    }

}