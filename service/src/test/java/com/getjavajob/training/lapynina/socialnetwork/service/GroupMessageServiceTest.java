package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.MessageGroup;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.GroupMessageRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import com.getjavajob.training.lapynina.socialnetwork.service.resources.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ServiceConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@AutoConfigureMockMvc
public class GroupMessageServiceTest {
    @InjectMocks
    GroupMessageService messageService;
    @Mock
    private MessageValidator validator;
    @Mock
    private GroupMessageRepositoryImp groupMessageRepositoryImp;

    private MessageGroup createMessage() {
        return MessageGroup.builder()
                .recipientId(1)
                .senderId(2)
                .textMessage("hi")
                .appointment("wall").build();
    }

    @Test
    public void createTest() throws ValidateException {
        MessageGroup messageGroup = createMessage();
        when(groupMessageRepositoryImp.create(messageGroup)).thenReturn(messageGroup);
        assertEquals(messageGroup, messageService.create(messageGroup));
    }

    @Test
    public void updateTest() throws ValidateException, SQLException {
        MessageGroup messageGroup = createMessage();
        when(groupMessageRepositoryImp.create(messageGroup)).thenReturn(messageGroup);
        when(groupMessageRepositoryImp.getById(messageGroup.getId())).thenReturn(messageGroup);
        assertEquals(messageGroup, messageService.update(messageGroup));
    }

    @Test
    public void getByIdTest() {
        MessageGroup messageGroup = createMessage();
        when(groupMessageRepositoryImp.getById(messageGroup.getId())).thenReturn(messageGroup);
        assertEquals(messageGroup, messageService.getById(messageGroup.getId()));
    }

    @Test
    public void deleteByIdTest() {
        MessageGroup messageGroup = createMessage();
        when(groupMessageRepositoryImp.deleteById(messageGroup.getId())).thenReturn(true);
        assertTrue(messageService.deleteById(messageGroup.getId()));
    }

    @Test
    public void getByAppointmentTest() {
        MessageGroup messageGroup = createMessage();
        List<MessageGroup> messages = new ArrayList<>();
        messages.add(messageGroup);
        when(groupMessageRepositoryImp.getByAppointment(1, "wall")).thenReturn(messages);
        assertEquals(messages, messageService.getByAppointment(1, "wall"));
    }

}