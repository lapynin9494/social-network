package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.GroupMemberRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.GroupRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import com.getjavajob.training.lapynina.socialnetwork.service.resources.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ServiceConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@AutoConfigureMockMvc
public class GroupServiceTest {
    @InjectMocks
    GroupService groupService;
    @Mock
    private GroupRepositoryImp groupRepositoryImp;
    @Mock
    private GroupMemberRepositoryImp groupMemberRepositoryImp;
    @Mock
    private GroupValidator validator;

    private Group createGroup() {
        return Group.builder()
                .name("Java")
                .description("sport")
                .idCreator(1)
                .creationDate(LocalDate.parse("2005-10-21"))
                .build();
    }

    @Test
    public void createGroupTest() throws ValidateException {
        Group group = createGroup();
        when(groupRepositoryImp.create(group)).thenReturn(group);
        assertEquals(group, groupService.create(group));
    }

    @Test
    public void updateGroupTest() throws ValidateException {
        Group group = createGroup();
        when(groupRepositoryImp.create(group)).thenReturn(group);
        when(groupRepositoryImp.getById(group.getId())).thenReturn(group);
        assertEquals(group, groupService.update(group));
    }

    @Test
    public void getAllTest() {
        Group group = createGroup();
        List<Group> groups = new ArrayList<>();
        groups.add(group);
        when(groupRepositoryImp.getAll()).thenReturn(groups);
        assertEquals(groups, groupService.getAll());
    }

    @Test
    public void getByIdTest() {
        Group group = createGroup();
        when(groupRepositoryImp.getById(group.getId())).thenReturn(group);
        assertEquals(group, groupService.getById(group.getId()));
    }

    @Test
    public void deleteByIdTest() {
        Group group = createGroup();
        when(groupRepositoryImp.deleteById(group.getId())).thenReturn(true);
        assertTrue(groupService.deleteById(group.getId()));
    }

    @Test
    public void getMembersTest() {
        Group group = createGroup();
        List<Account> groups = group.getMembers();
        when(groupMemberRepositoryImp.getMembers(group.getId())).thenReturn(group);
        assertEquals(groups, groupService.getMembers(group.getId()));
    }

    @Test
    public void getInvitesTest() {
        Group group = createGroup();
        List<Account> accountList = group.getInviter();
        when(groupMemberRepositoryImp.getInvitesAccount(group.getId())).thenReturn(accountList);
        assertEquals(accountList, groupService.getInvites(group.getId()));
    }

}