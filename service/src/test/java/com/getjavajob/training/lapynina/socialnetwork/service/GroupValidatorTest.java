package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Group;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import com.getjavajob.training.lapynina.socialnetwork.service.resources.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = ServiceConfig.class)
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.main.lazy-initialization=true")
public class GroupValidatorTest {
    @Autowired
    GroupValidator validator;

    private Group createGroup() {
        return Group.builder()
                .name("Java")
                .description("sport")
                .idCreator(1)
                .creationDate(LocalDate.parse("2005-10-21"))
                .build();
    }

    @Test
    public void validateTrueTest() throws ValidateException {
        Group group = createGroup();
        assertEquals(group, validator.validateGroup(group));
    }

    @Test(expected = ValidateException.class)
    public void validateFalseTest() throws ValidateException {
        Group group = createGroup();
        group.setName(null);
        validator.validateGroup(group);
    }

}