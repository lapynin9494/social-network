package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.common.News;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.NewsRepository;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.MessageRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.ValidateException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageValidator validator;
    private MessageRepositoryImp messageRepositoryImp;
    private NewsRepository newsRepository;
    private AccountService accountService;

    @Transactional
    public Message create(Message message) throws ValidateException {
        Message messageData = messageRepositoryImp.create(message);
        List<Account> friendsList = accountService.getFriends(message.getSenderId());
        if (message.getAppointment().equals("wall")) {
            News news = new News();
            news.setId(messageData.getId());
            news.setTimeSend(messageData.getTimeSend().format(ofPattern("MMddHHmmss")));
            newsRepository.save(friendsList, news);
        }
        return messageData;
    }

    @Transactional
    public Message getById(int messageId) {
        return messageRepositoryImp.getById(messageId);
    }

    @Transactional
    public Message update(Message messageRequest) throws SQLException, ValidateException {
        validator.validateMessage(messageRequest);
        messageRepositoryImp.update(messageRequest);
        return messageRepositoryImp.getById(messageRequest.getId());
    }

    @Transactional
    public List<Message> getByAppointment(int recipientI, String appointment) {
        return messageRepositoryImp.getByAppointment(recipientI, appointment);
    }

    @Transactional
    public List<Message> getPrivateDialog(int recipientId, int senderId) {
        return messageRepositoryImp.getByRecipientIdAAndSenderIdAndAppointment(recipientId, senderId, "personal");
    }

    @Transactional
    public boolean deleteById(int id) {
        messageRepositoryImp.deleteById(id);
        return true;
    }

}