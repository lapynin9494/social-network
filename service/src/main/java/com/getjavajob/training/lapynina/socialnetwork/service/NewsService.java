package com.getjavajob.training.lapynina.socialnetwork.service;

import com.getjavajob.training.lapynina.socialnetwork.common.Account;
import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.NewsRepository;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.AccountMemberRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.datajpa.repository.AccountRepositoryImp;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.MessageDto;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.MessageMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class NewsService {
    private NewsRepository newsRepository;
    private AccountRepositoryImp accountRepositoryImp;
    private AccountMemberRepositoryImp accountMemberRepositoryImp;
    private MessageMapper messageMapper;

    private Set<Integer> getNewsIdForUser(int userId, int page) {
        return newsRepository.getNewsForUser(userId, page);
    }

    public List<String> getNewsForUser(int userId, int page) {
        Set<Integer> newsId = getNewsIdForUser(userId, page);
        return newsId.stream().map(Object::toString).collect(Collectors.toList());
    }

    public List<MessageDto> getAllNewsByFriends(int idUser) {
        List<Account> friendsUser = accountMemberRepositoryImp.getMembers(idUser).getAccountFriends();
        List<Message> messageAllNewsList = new ArrayList<>();
        for (Account friend : friendsUser) {
            List<Message> messageFriendNews = accountRepositoryImp.getAllNewsByFriends(friend.getId());
            messageAllNewsList.addAll(messageFriendNews);
        }
        List<MessageDto> messageDtoAllNewsList = messageAllNewsList.stream().map(message -> messageMapper.messageToService(message))
                .collect(Collectors.toList());
        return messageDtoAllNewsList;
    }

}