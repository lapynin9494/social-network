import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class Log4j2Test {
    private static final Logger logger = LogManager.getLogger();

    @Test
    public void logTest() {
        logger.debug("DEBUG");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
    }

}