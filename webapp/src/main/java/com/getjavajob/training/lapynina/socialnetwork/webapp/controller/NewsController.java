package com.getjavajob.training.lapynina.socialnetwork.webapp.controller;

import com.getjavajob.training.lapynina.socialnetwork.common.Message;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.MessageDto;
import com.getjavajob.training.lapynina.socialnetwork.dao.dto.util.MessageMapper;
import com.getjavajob.training.lapynina.socialnetwork.service.MessageService;
import com.getjavajob.training.lapynina.socialnetwork.service.NewsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
@RequestMapping("/account/news")
public class NewsController {
    private NewsService newsService;
    private MessageService messageService;
    private MessageMapper messageMapper;


    @GetMapping("/{page}")
    public ModelAndView getNews(Model model, @RequestParam("id") int userId, @PathVariable("page") int page) {
        List<Message> messages = newsService.getNewsForUser(userId, page).stream().map(message -> messageService.getById(Integer.parseInt(message))).collect(Collectors.toList());
        List<MessageDto> messagesDto = messages.stream().map(message -> messageMapper.messageToService(message)).collect(Collectors.toList());
        model.addAttribute("messagesNews", messagesDto);
        int newsCount = (messagesDto.size() / 5) + 1;
        model.addAttribute("newsCount", newsCount);
        model.addAttribute("accountId", userId);
        return new ModelAndView("/account/account-news");
    }

    @PostMapping("/all")
    public ModelAndView getAllNews(Model model, @RequestParam("id") int userId) {
        List<MessageDto> messagesAllNews = newsService.getAllNewsByFriends(userId);
        model.addAttribute("messagesNews", messagesAllNews);
        model.addAttribute("newsCount", 1);
        model.addAttribute("accountId", userId);
        return new ModelAndView("/account/account-news");
    }

}