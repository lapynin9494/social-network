<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Новости</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/common/header.jsp"/>
<div class="container mt-1">
    <div class="row">
        <div class="col">
            <h3>Новости друзей</h3>
            <c:forEach var="message" items="${messagesNews}">
                <div class="row">
                    <div class="col-9">
                        <div class="alert alert-primary"
                             role="alert">${message.textMessage}</div>
                    </div>
                    <div class="col">${message.timeSend}
                        <a href="${pageContext.request.contextPath}/account/info?id=${message.senderId}"
                           class="text-decoration-none">${message.accountSender.name} ${message.accountSender.surname}</a>
                    </div>
                </div>
            </c:forEach>
            <div class="pagination">
                <c:forEach var="page" begin="01" end="${newsCount}">
                    <a class="page-link"
                       href="${pageContext.request.contextPath}/account/news/${page}?id=${accountId}"> ${page}</a>
                </c:forEach>
            </div>
            <br>
            <form:form action="${pageContext.request.contextPath}/account/news/all"
                       method="post">
                <input type="hidden" name="id" value="${userId}">
                <td><input type="submit" class="btn btn-primary" id="allNews"
                           value="все новости"/></td>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>