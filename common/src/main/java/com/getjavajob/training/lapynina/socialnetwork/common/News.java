package com.getjavajob.training.lapynina.socialnetwork.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@RedisHash("News")
@Component
@NoArgsConstructor
public class News implements Serializable {

    private int id;
    private String timeSend;

}